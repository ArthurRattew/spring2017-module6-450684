Hello!
Assumptions made in this project: When loading webpage that does not exit, the wiki says:
"Visiting a file that does not exist inside the static directory results in a 404"
My website produces this error (you can check from the network tab!),
however, I chose not to have 'error 404' explicitly printed it to the screen. This
assumption is fine according to the professor's comment on the following Piazza post:
https://piazza.com/class/ixt8h6xhjof157?cid=601

INDIVIDUAL PORTION:
Links to:
college.html: http://ec2-35-162-197-132.us-west-2.compute.amazonaws.com:3456/individual/static/college.html
brookings.jpg:http://ec2-35-162-197-132.us-west-2.compute.amazonaws.com:3456/individual/static/brookings.jpg
hello.txt: http://ec2-35-162-197-132.us-west-2.compute.amazonaws.com:3456/individual/static/hello.txt

Why phpinfo.php behaves the way it does when loaded through node.js:
phpinfo.php behaves this way when loaded through node.js, because it doesn't have
Apache to serve it. Since the server can't process the .php file, it simply sends
the file to the browser. Most popular browsers such as chrome and firefox, then either
download or prompt the user to save the .php file instead of displaying it in the
browser window.

ok



GROUP PORTION:
Assumptions made:
-As stated on Piazza, I only used jQuery for manipulating elements in the DOM. It was
not used to communicate, send or receive data from the server. Piazza post
this assumption is stated at: https://piazza.com/class/ixt8h6xhjof157?cid=633


Creative portion:
-Created a flag system that allows users to get kicked permanently from the room.
-Users can choose their own username (not required by assignemnt)


***link correction, the link for the group portion is: http://ec2-35-162-197-132.us-west-2.compute.amazonaws.com:3456/group/client.html ***
however, as mentioned on piazza, simply uploading the assignment files is enough as we are  not expected to keep our server running