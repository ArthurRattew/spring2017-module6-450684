// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");


var nameArray = [];

var usrUniqueToken;
var usrUniqueTokenSet = new Set();

//get randomInt function inspired by: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function idSystem(){
	//GET TOKEN FROM CLIENT HERE, CHECK IF IT EXISTS (ELSE CREATE) THEN CHECK IF IT BLACKLISTED (THEN KICK) (ELSE PROCEED)
	usrUniqueToken = "";
	for (var i =0; i < 10; i++){
		usrUniqueToken += getRandomInt(0,9).toString();
	}
	//console.log(usrUniqueToken);
	while (usrUniqueTokenSet.has(usrUniqueToken))
	{
		usrUniqueToken += getRandomInt(0,9).toString();//if the UNIQUE identifier has already been added to the uniquetoken set, random nums are appended until it isnt
	}
	usrUniqueTokenSet.add(usrUniqueToken);
	return usrUniqueToken;
}

var counter = 0;
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){// This callback runs when a new connection is made to our HTTP server.

	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.

		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);

	});
});



//universal set
var setU = new Set();//set of ALL people currently using the chat service

var setBlackListRoom = new Set(); //BLACK LIST OF USERS FOR A GIVEN ROOM

var allRoom = [1];//ONE ROOM INITIALLY, CALLED lobby ROOM
allRoom[0] = "lobby";

var setAllRoomNames = new Set();

var indexTracker = 0;
var rmNameToIndex = {};

rmNameToIndex["lobby"] = indexTracker.toString();//POSSIBLE SWITCHING TO SIMPLER ARRAY SOL'N
indexTracker++;

setAllRoomNames.add("lobby");

//NOT CURRENTLY IN USE, REPLACED BY 2D ARRAY
var setInRoom = new Set();//a set within every room element, contains the users currently in that room


//-------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------



//var associativeArray = {};//I'm aware this is an object,
var associativeMap = new Map();
var keyObj = {};

// keyAr = new Array(300);//names of all rooms
// valAr = new Array(300);

var height = 100;

var keyAr = Array(height).fill(null);
var valAr = Array(40).fill(null);

var container = Array(height).fill(null);
for (var val in keyAr){

	var ret = associativeMap.set(keyAr[val], valAr);
	container[val] = ret;


}

//container NOW CONTAINS A "2D-ARRAY" THAT CAN BE USED TO MAP USERS TO GIVEN ROOMS
//container[i] contains key-value pair
//console.log((container[0])); see this for example


// keyAr[0]="room1";
// valAr[0]="user1";
//console.log(associativeMap.get(keyAr));

//however prototypal languages are the worst thing on the planet, so I'm gonna pretend this is an array
// associativeArray["NAME OF KEY HERE"] = [""];
// associativeArray["lobby"]=["",""];

// var theArray = {};
// theArray["manager"] = ["Prateek","Rudresh","Prashant"];
// theArray["employee"] = ["namit","amit","sushil"];
// theArray["hr"] = ["priya","seema","nakul"];
// theArray["hr"]="4";
// theArray["hr"][2] = "5";
// console.log(theArray["hr"][2]);







app.listen(3456);
var people = {};
// Do the Socket.IO magic:
var io = socketio.listen(app);


io.sockets.on("connection", function(socket){
	//--------------------------------
	socket.on("join", function(name){
			people[socket.id] = name.room;
			console.log("name: " + name.room);
			socket.emit("update", "You have connected to the server.");
			io.sockets.emit("update", name.room + " has joined the server.")
			io.sockets.emit("update-people", people);
	});

	socket.on("send", function(msg){
			io.sockets.emit("chat", people[socket.id], msg);
	});


	socket.on("disconnect", function(data){
			socket.leave(data.room);
			io.sockets.emit("update", people[socket.id] + " has left the server.");
			delete people[socket.id];
			io.sockets.emit("update-people", people);
	});

	//-----------

	socket.on('message_to_serverName', function(data) {
		// io.sockets.emit("message_to_name",{"usrname":data["name"]}); // broadcast the message to other users
		var tempName = data["name"];
		while (setU.has(tempName))
		{
			//console.log(inSet);
			tempName += getRandomInt(0,9).toString();
		}
		nameArray[tempName.length] =tempName;
		//ADD TO SERVER SIDE CODE HERE WHAT THE CURRENT ROOM ASSOCIATED WITH THAT USERNAME IS, PREVENTS CHANGING THE JS
		setInRoom.add(tempName);
		setU.add(tempName);

		// console.log(tempName);
		io.sockets.emit("message_to_name", {"usrname":tempName});
	});

	//creating response to client asking for token
	socket.on('message_to_initiate_token', function(data){
		var uniqueId = idSystem();
		// console.log(uniqueId);
		socket.emit("usrIdentifier", {"usrToken": uniqueId}); // broadcast the message to other users
	});


	socket.on('updatePpl', function(data) {
		io.sockets.emit("updated-people", people);
	});

	socket.on('message_to_serverRoomUpd', function(data) {//recieved and sends room name to server
		var valRoom = data["rmName"];
		data.room = valRoom;
		// console.log(socket.id);
		// console.log(people);
		people[socket.id] = data.room;
		// console.log("name: " + data.room);
		socket.emit("update", "You have connected to the server.");
		io.sockets.emit("update", data.room + " has joined the server.")
		io.sockets.emit("update-people", people);
		console.log(people);


		// console.log(valRoom);
		//check if room name has been created already, or if it needs to be created
		if(setAllRoomNames.has(valRoom)){}//room already created, thus simply do nothing
		else{//the room was not already created
			//add the room to the room array and to the set of all rooms
			setAllRoomNames.add(valRoom);
			allRoom[allRoom.length] = valRoom;

		}

		//container[container.length] = valRoom;
		var curIndex = allRoom.length-1;
		keyAr[curIndex] = valRoom;

		//console.log(keyAr[curIndex]);

		for (var i =0; i <= curIndex; i++){//intetionally <=
			if ((valAr[i] == null || valAr[i] == "")&&!(valAr.includes(data["usrsName"]))){
				valAr[i] =data["usrsName"];
				break;
			}
		}

		var mapV2 = new Map();
		mapV2.set(keyAr[curIndex], valAr);//MAP VS CONTAINS A KEY VALUE PAIRING BETWEEN THE NAME OF THE ROOM AND THE NAME OF THE USERS

		//console.log(valAr);
		io.sockets.emit("message_to_ah",{"rmNum":valRoom});//send room number
		io.sockets.emit("message_with_allRooms", {"rmArray":allRoom,"rmNum":valRoom, "valAr":valAr, "keyAr":keyAr[curIndex].toString()});

	});

	// broadcast the message to other users
	//io.sockets.emit("message_to_ah",{"rmNum":allRoom[counter]}) ;//send room number

	socket.on('message_to_serverMessage', function(data) {
		var roomPostIn = data.roomNamePasser;
		// alert(roomPostIn);
		io.sockets.emit("message_to_client",{message:data["message"], "posterNameRec":data.posterName}); // broadcast the message to other users
	});// This callback runs when the server receives a new message from the client.
		// This callback runs when a new Socket.IO connection is established.
	});






//legacy:
	//GET HEIGHT (Y) INDEX BY: (NOTE THAT FOLLOWING DESCRIBED APPROACH IS NO LONGER IS USED, BUT HAS BEEN LEFT HERE AS LEGACY/ FUTURE REFERENCE)
	//	-->SINCE ROOM ARE NEVER DELETED, IF ADDED IMMEDIATELY TO NEWEST FREE Y INDEX, 1-1 CORRESPONDANCE OF SET ELEMENT CREATION AND ARRAY INDEX
	//	-->THUS CREATE A MAP THAT MAPS THE ROOM NAME TO THE INDEX IT WAS PLACE IN, AND THEN RECIEVE INDEX FOR ARRAY FROM MAP SINCE JAVASCRIPT IS SILLY
	//	-->USE AN OBJECT AS THE MAP/NAMEDKEYS

// var curHeight = allRoom.indexOf(valRoom);
//
// console.log(arrayOfSets[curHeight]);
//
// function isInArray(value){
// 	return (arrayOfSets[curHeight].indexOf(value)>=0);
// }
//
// // var isIn = isInArray(usersName, arrayOfSets[curHeight]);
// var isIn = isInArray(usersName);
// console.log(isIn);
//
//
//
// if (isIn){
// 	//array already contains the user, thus no need to include twice
// }
// else{//the array does not contain the user, add the the index correspending to the room just commented in
// 	arrayOfSets[arrayOfSets.length][arrayOfSets[curHeight].length] = usersName;//where first index y, (array[Y][X]) represents the room
// }
//
// console.log(arrayOfSets);









	// pushing bottom down for visibility
